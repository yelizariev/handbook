# Developer
* Work week -- 5 days
* Weekly workload (minimum hours) is 5 hours per work day, i.e. 25 hours in a normal week without holidays and vacations. Any type of projects are counted
* Each hour after 20 hours has extra 50% compensation, but not more than for 10 hours. Only non hourly-rated projects are counted.
* Work on hourly-rated projects has extra 50% compensation.

## Take care of your time

### Stop and ask

* if you faced a problem and cannot find a solution more than 2 hours, then stop and wait for consultation with more experienced developers
* after [reporting on not working advice](messaging.md#report-on-not-working-advice), it's probably better to wait while your advicer check that it *really* doesn't work, because oftenly advice was just wrongly applied
