# No micromanagement
Avoid micromanagement

* Practice of autonomous work with probably a little bit far from *ideal* result is better than *ideal* but with micromanagement

# No rushing

Super urgent tasks is a sign of bad planning

* After each case rethink what was the reason and how to avoid it

# Respectful communication

* Don't use rhetorical questions. They don't help to analyze a problem and move forward.
* Don't use pronouns (*he*, *she*). At least when the person can hear what you said or read what you wrote

# Be open for community
* twitter

    * you can use your account to tweet about development, work, traveling, vacation, fun
    * twitter profile's website URL has to be your page at team section, e.g. it-projects.info/team/yelizariev
    * no requirements for profile description, header photo
    * profile photo should be a real face photo
    * location has to be specifed ``YouCity, Country``

* github

    * https://github.com/settings/profile

      * public email: unselected
      * URL must be link to public profile at company's website
      * Company must be set to ``@it-projects-llc``
      * Location must be your ``YouCity, Country``
      * Photo must be your real face photo

    * https://github.com/settings/emails

      * primary email must be personal address …@it-projects.info
      * “Keep my email address private” must be switched off

    * https://github.com/orgs/it-projects-llc/people

      * get invitation
      * set ``Organization visibility`` to ``Public``

* https://gravatar.com

  * Be sure that your company email has avatar
